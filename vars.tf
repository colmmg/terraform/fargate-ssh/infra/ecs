variable "ecs-public-key" {
  description = "The public key for the ECS keypair."
}

variable "my-ip" {
  description = "Your public IP address."
  default     = "10.0.0.1"
}

variable "region" {
  description = "The AWS region."
  default     = "us-east-1"
}
