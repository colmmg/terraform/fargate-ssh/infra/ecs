resource "aws_service_discovery_private_dns_namespace" "private" {
  name        = "local"
  description = "Internal private namespace"
  vpc         = aws_default_vpc.default.id
}
