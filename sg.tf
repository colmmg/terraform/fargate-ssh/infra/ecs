resource "aws_security_group" "ecs-web" {
  description = "Security group for web applications in ECS"
  name        = "ecs-web"
  tags = {
    Name    = "ecs-web"
  }
  vpc_id = aws_default_vpc.default.id
}

resource "aws_security_group_rule" "ecs-web-80" {
  cidr_blocks       = ["0.0.0.0/0"]
  description       = "HTTP access from the world."
  from_port         = 80
  protocol          = "tcp"
  security_group_id = aws_security_group.ecs-web.id
  to_port           = 80
  type              = "ingress"
}

resource "aws_security_group_rule" "ecs-web-outbound" {
  cidr_blocks       = ["0.0.0.0/0"]
  description       = "Full outbound access"
  from_port         = 0
  protocol          = "-1"
  security_group_id = aws_security_group.ecs-web.id
  to_port           = 0
  type              = "egress"
}

resource "aws_security_group" "ecs-jump" {
  description = "Security group for ECS jumpbox."
  name        = "ecs-jump"
  tags = {
    Name    = "ecs-jump"
  }
  vpc_id = aws_default_vpc.default.id
}

resource "aws_security_group_rule" "ecs-jump-22" {
  cidr_blocks       = ["${var.my-ip}/32"]
  description       = "SSH access from the MyIp."
  from_port         = 22
  protocol          = "tcp"
  security_group_id = aws_security_group.ecs-jump.id
  to_port           = 22
  type              = "ingress"
}

resource "aws_security_group_rule" "ecs-jump-outbound" {
  cidr_blocks       = ["0.0.0.0/0"]
  description       = "Full outbound access"
  from_port         = 0
  protocol          = "-1"
  security_group_id = aws_security_group.ecs-jump.id
  to_port           = 0
  type              = "egress"
}

resource "aws_security_group_rule" "ecs-web-jump" {
  description              = "SSH access from ECS jumpbox."
  from_port                = 22
  protocol                 = "tcp"
  security_group_id        = aws_security_group.ecs-web.id
  source_security_group_id = aws_security_group.ecs-jump.id
  to_port                  = 22
  type                     = "ingress"
}
