data "aws_ami" "amzn2" {
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-2.0.*"]
  }
  most_recent = true
  owners      = ["137112412989"]
}

resource "aws_key_pair" "ecs" {
  key_name   = "ecs"
  public_key = var.ecs-public-key
}

data "template_file" "user-data" {
  template = file("${path.module}/templates/user_data.tpl")
}

resource "aws_instance" "ecs-jumpbox" {
  ami                    = data.aws_ami.amzn2.id
  instance_type          = "t3a.nano"
  key_name               = "ecs"
  user_data              = data.template_file.user-data.rendered
  vpc_security_group_ids = [aws_security_group.ecs-jump.id]
  tags = {
    "Name" = "ecs-jumpbox"
  }
}

