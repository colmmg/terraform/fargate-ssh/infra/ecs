data "aws_iam_policy" "ecs-task-exec-role-policy" {
  arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}

data "aws_iam_policy_document" "ecs-tasks" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type = "Service"
      identifiers = [
        "ecs-tasks.amazonaws.com"
      ]
    }
  }
}

resource "aws_iam_role" "ecs-task-exec-role" {
  assume_role_policy = data.aws_iam_policy_document.ecs-tasks.json
  name               = "ecsTaskExecutionRole"
  tags = {
    Name    = "ecsTaskExecutionRole"
  }
}

resource "aws_iam_role_policy_attachment" "ecs-task-exec-role-policy" {
  policy_arn = data.aws_iam_policy.ecs-task-exec-role-policy.arn
  role       = aws_iam_role.ecs-task-exec-role.name
}
