output "ecs-public-key" {
  value = var.ecs-public-key
}

output "ecs-web-sg-id" {
  value = aws_security_group.ecs-web.id
}

output "private-namespace-id" {
  value = aws_service_discovery_private_dns_namespace.private.id
}
