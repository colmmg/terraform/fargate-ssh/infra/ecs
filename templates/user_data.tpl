#!/bin/bash

mkdir /home/ec2-user/.ssh
chown ec2-user: /home/ec2-user/.ssh
cat <<EOF > /home/ec2-user/.ssh/id_rsa
INSERT PRIVATE KEY HERE
EOF
chown ec2-user: /home/ec2-user/.ssh/id_rsa
chmod 400 /home/ec2-user/.ssh/id_rsa
